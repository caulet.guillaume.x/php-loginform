<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="style.css" />
    <title>Login Form</title>
</head>

<body>
    <div class="container">
        <form autocomplete="off" action="login.php" method="post">

            <div class="bloc">
                <label for="pseudo"><b>Pseudo</b></label>
                <input type="text" placeholder="Entrez votre Pseudo" name="pseudo" required>

                <label for="mdp"><b>Mot de Passe</b></label>
                <input type="text" placeholder="Entrez votre Mot de Passe" name="mdp" required>

                <button type="submit" name="login">Connexion</button>
                <label>
            </div>
        </form>
    </div>
</body>

</html>