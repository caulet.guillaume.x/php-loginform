<?php
session_start();
session_destroy();

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="refresh" content="3; URL=index.php" />
    <title>Déconnexion</title>
</head>

<body>
    <p>Bye Bye</p>
</body>

</html>